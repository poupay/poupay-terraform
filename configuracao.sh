#!/bin/bash

sudo apt update -y && sudo apt-get upgrade -y

#Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get install docker-ce -y

sudo adduser ubuntu docker

#Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

git config --global user.name "poupay-bot"
git config --global user.email "poupay-bot"

git clone https://gitlab.com/poupay/poupay-python.git ./python

#pull image bd
docker run 